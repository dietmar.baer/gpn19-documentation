#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
# E-Mail:       christoph.stoettner@stoeps.de
#
# Create presentation
for i in ls /documents/*.adoc
do
  asciidoctor \
      -r asciidoctor-diagram \
      -r asciidoctor-mathematical \
      -a toc=left \
      "$i"
done

