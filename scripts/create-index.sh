#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
#

ROOT=public
HTTP="https://stoeps.gitlab.io/gpn19-documentation"
OUTPUT="public/index.html"

i=0
echo "<html>" > $OUTPUT
echo "<head>" >> $OUTPUT
echo "<title>Documentation</title>" >> $OUTPUT
echo "<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/light.css\">" >> $OUTPUT
echo "</head>" >> $OUTPUT
echo "<body>" >> $OUTPUT
echo "<h1>Documentation</h1>" >> $OUTPUT
for filepath in $(find "$ROOT" -maxdepth 1 -mindepth 1 -type d| grep -v images | sort)
do
  path=$(basename "$filepath")
  echo "<div class=\"block\">" >> $OUTPUT
  echo "<h2>$path</h2>" >> $OUTPUT
  echo "  <ul>" >> $OUTPUT
  for i in $(find "$filepath" -maxdepth 1 -mindepth 1 -type f | grep \.html |grep -v .placeholder | grep -v stem- | sort)
  do
    file=$(basename "$i")
    echo "    <li><a href=\"$HTTP/$path/$file\" target=\"_blank\">$file</a></li>" >> $OUTPUT
  done
  for i in $(find "$filepath" -maxdepth 1 -mindepth 1 -type f | grep \.pdf | grep -v .placeholder | sort)
  do
    file=$(basename "$i")
    echo "    <li><a href=\"$HTTP/$path/$file\" target=\"_blank\">$file</a></li>" >> $OUTPUT
  done
  echo "  </ul>" >> $OUTPUT
  echo "</div>" >> $OUTPUT
done
echo "</body></html>" >> $OUTPUT
